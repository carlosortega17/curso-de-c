// standard input output
#include <stdio.h>

// Funcion principal, el primer parametro es el numero de argumentos y el segundo parametro son los argumentos en un arreglo de strings
int main(int argc, char** argv) {
  // Imprimir en consola el mensaje hola con un salto de linea
  printf("hola\n");
  return 0;
}
