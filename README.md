# Curso de C
### Es necesario tener clang instalado para poder compilar los ejercicios

- 1. Estructura basica de un programa en C

```bash
make program1
```

- 2. Tipos de datos

```bash
make program2
```

- 3. Calculadora

```bash
make program3
```

- 4. Scanf (Leer datos de consola)

```bash
make program4
```
