#include <stdio.h>

int main(int argc, char** argv) {
  int numero = 0;
  printf("Escribe un numero: ");
  scanf("%i", &numero); // Para poder ingresar numeros con scanf es necesario obtener la direccion de memoria con &
  printf("El numero es: %i\n", numero);
  return 0;
}
