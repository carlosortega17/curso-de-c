#include <stdio.h>

int main(int argc, char** argv) {
  printf("Tipos de datos\n");
  // Numeros enteros
  int numero = 10;
  // Punto decimal
  float flotante = 10.4;
  double puntoflotante = 10.23;
  // Caracter
  char caracter = 'q';
  // boleano
  int verdadero = 1;
  int falso = 0;
  // texto
  char nombre[] = "Hola";

  printf("Numero: %i\n", numero);
  printf("Punto flotante (float): %.2f\n", flotante);
  printf("Punto flotante (double): %.2f\n", puntoflotante);
  printf("Caracter: %c\n", caracter);
  printf("Texto: %s\n", nombre);

  return 0;
}
