#include <stdio.h>

int main(int argc, char** argv) {
  printf("Suma de 2 numeros\n");
  int numero1 = 10;
  int numero2 = 20;
  int numero3 = numero1 + numero2;
  printf("El total de %i + %i = %i\n", numero1, numero2, numero3);
  return 0;
}
